#! /bin/sh
#################
# Script for
# Listing all users on a system (without root obviously) 
################


awk -F: '{if ($3 >= 1000 && $3 != 65534) print $1}' /etc/passwd


#eigentlich die richtigere Lösung:
# getent passwd | awk -F: "{if (\$3 >= $(awk '/^UID_MIN/ {print $2}' /etc/login.defs) && \$3 <= $(awk '/^UID_MAX/ {print $2}' /etc/login.defs)) print \$1}"
