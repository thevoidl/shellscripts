#! /bin/sh
# PDF komprimieren

#komprimierungslevel 1-4

if [[ $2 -eq 1 ]]; then #72dpi
  gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=komprimiert.pdf $1
else if [[ $2 -eq 2 ]]; then #150dpi
  gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer -dNOPAUSE -dQUIET -dBATCH -sOutputFile=komprimiert.pdf $1
else if [[ $2 -eq 3 ]]; then #300dpi
  gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=komprimiert.pdf $1
else if [[ $2 -eq 4 ]]; then #300dpi + color preserving
  gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=komprimiert.pdf $1
fi
