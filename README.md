Disclaimer
---
**HowTo**: I am sourcing the file `aliases` in my `~/.zshrc`. That's all
 
# My Setup
 * OS: Manjaro (Linux Arch)
 * shell: zsh
-> Some aliases or script won't make sense on other machines

# Different specialities
 * Some scripts are for use at Salzburg University of Applied Science only
 * Most aliases are hardcodedfor my own environment
