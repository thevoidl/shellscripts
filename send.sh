#!/bin/zsh
#################
# Script for
# Sending files on the LAN 
################

PORT=1234
DEST=192.168.100.249

###--> Hilfe anzeigen
if [[ $# -eq 0 ]] || [[ ! -s $1 ]]; then
cat <<EOF
Dateien oder Ordner mit netcat und tar im lokalen Netz verschicken.
  BENUTZUNG:
           $0 [FILE/FOLDER] (DEST_IP) (PORT)
  Wird zu:
           "tar -cf - [FOLDER] | nc [DEST_IP] [PORT]"
  Standardwerte:
           $DEST:$PORT
EOF
exit 0
fi

###--> Wenn 2 Argumente, dann ist $1 [Pfad] und $2 [destination_IP]:
if [[ $# -gt 1 ]]; then
###--> IP parsen
  if [[ $(echo $2) =~ "192\\.168\\.([0-9]|[0-9][0-9]|[0-2][0-9][0-9])\\.([0-9]|[0-9][0-9]|[0-2][0-9][0-9])" ]]; then
    DEST="$2"
  else
    echo "$2 ist keine lokale IP-Adresse"
    exit 1
  fi
###--> PORT parsen
  if [[ $3 -gt 999 ]] && [[ $3 -lt 65536 ]]; then
    PORT="$3"
  fi
fi

echo "Sende \"$1\" über Port $PORT an $DEST"

tar -cf - $1 | nc -c $DEST $PORT
RES=$?

if [[ ! $RES -eq 0 ]]; then
  echo natcat Fehler $RES!
  exit $RES
else
  echo Fertig!
fi
