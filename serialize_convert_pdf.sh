#! /bin/sh

for file in $(ls | grep pnm); do
    convert $file $(echo $(echo $file | awk -F. '{print $1}').pdf)
    rm $file
    echo $file done
done
