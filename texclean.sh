#!/bin/zsh
# Clean all tex build-files

cd $PWD

rm **/*.aux
rm **/*.bbl
rm **/*.bcf
rm **/*.blg
rm **/*.lof
rm **/*.log
rm **/*.lol
rm **/*.lot
rm **/*.out
rm **/*.pdf
rm **/*.xml
rm **/*.toc
rm **/*.gz
