#! /bin/zsh
#################
# Script for
# Mounting the windows share from my win7 VM 
################
trap onExit 1 32
set -e
function onExit() {
  rmdir $LOCAL_DIR
}

#VM IP
  IP="192.168.100.144"
#The user that is allowed to access the share
  WIN_USER=valentin
#Remote windows dir
  DIR_TO_MOUT="Users/Public"
#Local dir to link the share to
  LOCAL_DIR="$HOME/Schreibtisch/share"

if [[ $# -eq 1 ]]; then
  if [[ $1 =~ ^192\.168\.([0-9]|[0-9][0-9]|[0-2][0-9][0-9])\.([0-9]|[0-9][0-9]|[0-2][0-9][0-9])$ ]]; then
    IP="$1"
  else
    echo $1 ist keine lokale Adresse!
    exit 1
  fi
fi

[ -d $LOCAL_DIR ] || mkdir -p $LOCAL_DIR 
ping -c 1 -w 1 $IP >/dev/null || (echo $IP nicht erreichbar && exit 1)

sudo mount.cifs "//$IP/$DIR_TO_MOUT/" "$LOCAL_DIR/" -o rw,user=$WIN_USER,uid=$UID,gid=$GID

if [ $? -ne 0 ]; then
  echo 'Nope...'
  exit 1
fi


