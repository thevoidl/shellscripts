#! /bin/sh
######################
# Script for:
# Mounting my 2nd SSD
#####################

FH_DIR=/run/Datengrab/FH

sudo cryptsetup luksOpen /dev/sda1 Datengrab
#sudo udisksctl unlock -b /dev/sda1
sudo mkdir /run/Datengrab
sudo chown 1000:1000 /run/Datengrab
sudo mount /dev/mapper/Datengrab /run/Datengrab
sudo chown -R 1000:1000 /run/Datengrab
chmod -u-rw /run/Datengrab/Privat
sudo sysctl -p /etc/sysctl.d/ipv6deaktivieren.conf
