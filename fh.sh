#! /bin/zsh
#################
# Script for
# Opening thunar at FH_DIR
################

if [ -z ${FHDIR+x} ]; then
  . ~/.zshrc
  if [ -z ${FHDIR+x} ]; then
    echo '$FHDIR nicht definiert!'
    exit 1
  fi
fi

if [ ! -d /run/Datengrab ]; then
  $SHELLSKRIPTE/mnt.sh
fi

thunar /run/Datengrab/FH
