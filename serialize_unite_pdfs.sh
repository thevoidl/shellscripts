#! /bin/sh

declare -a LIST=()

for file in $(seq 1 $1); do
  tmp="$(echo $file).pdf "
  LIST+=$tmp
done

echo "pdfunite $LIST gesamt.pdf"
pdfunite $LIST gesamt.pdf

gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=komprimiert.pdf gesamt.pdf
