#! /bin/sh
#################
# Script for
# printing at university of applied sciences salzburg 
################

. ./fhCredentials

sudo systemctl start smb.service && smbclient -W fhs -U $FH_USERNAME //arpeggio.fhs.fh-salzburg.ac.at/FollowMe -m SMB2
sudo systemctl stop smb.service
