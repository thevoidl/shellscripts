#! /bin/sh
#################
# Script for
# Receiving files on the LAN 
################

PORT=1234
IP=$(ip addr | grep virbr | grep 192 | awk '{print $2}' | awk -F'/' '{print $1}')

echo "Warte an $IP:$PORT"
echo IP: $IP

NAME=$(nc -lp $PORT | tar xv)

echo "Empfangen: \"$NAME\""
