#!/bin/sh
# Calculate date in...
# e.g. define an alias "in":
#  >_ in 20 days
#  >_ In 20 days it will be
#  >_ 21.12.2010 13:13:13 CEST

# Avoid parsing wrong input
if [ $# -lt 1 ] || [ $# -gt 2 ]; then
    echo "I will not try to parse ´$@´"
    exit 1
fi


# Check if only a number is given
if [ $# -eq 1 ] && echo $1 | grep -qvE '[a-zA-Z]'; then
    echo "In $@ hours it will be..."
    ARGS="$@ hours"
else
    echo "In $@ it will be..."
    ARGS="$@"
fi

FINALDATE=$(date -d "`date '+%Y/%m/%d %H:%M:%S %Z'` +$ARGS")
# Exit if date cannot process command
if [ $? -ne 0 ]; then
    exit $?
fi

echo "  $FINALDATE"
exit 0
