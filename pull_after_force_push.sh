#!/bin/zsh

function pull_after_force_push() {
    BRANCH=`git branch --show-current`

    #read -p "This resets all local changes! Continue? [Y/n] " -n 1 -r
    read -q "REPLY?This resets all local changes! Continue? [Y/n] "
    echo
    
    if [[ $REPLY =~ ^[Yy]$ ]]; then
    ¦   git fetch origin
    ¦   git reset --hard origin/$BRANCH
    ¦   git pull
    fi
}
alias glf=pull_after_force_push